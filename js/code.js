/*Задача 1 
Використовуючи пробіли &nbsp; та зірочки * намалюйте використовуючи цикли

Порожній прямокутник*/
document.write ("<div>**********</div>");

for (let i=0; i<10; i++){
    document.write ("<div>*&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp*</div>");
}

document.write ("<div>**********</div>");
document.write ("<br/>");

//Трикутник

for (let i=0; i<10; i++){
    for (let a=i; a<10; a++){
        document.write ("<div class = triangle>*</div>");
    }
    document.write ("<br/>");
}

document.write ("<br/>");

//Ромб
let row = 1;

/*Задача 2 
Реалізувати програму на Javascript, яка буде знаходити всі числа кратні 5 (діляться на 5 без залишку) 
у заданому діапазоні.
- Вивести за допомогою модального вікна браузера число, яке введе користувач.
- Вивести в консолі усі числа кратні 5, від 0 до введеного користувачем числа. 
Якщо таких чисел немає - вивести в консоль фразу Sorry, no numbers
- Обов'язково необхідно використовувати синтаксис ES6 (ES2015) для створення змінних.

- Перевірити, чи введене значення є цілим числом. Якщо ця умова не дотримується, повторювати виведення вікна на екран, доки не буде введено ціле число.*/

let testNumber = parseFloat(prompt("Будь ласка, введіть ціле число."));
if (Number.isInteger(testNumber) === false){
    testNumber = parseFloat(prompt("Ви ввели не ціле число. Будь ласка, введіть ціле число."));
}

for (i=0; i<=testNumber; i++){
    if (i%5 === 0){
    console.log(i);
    } else if (testNumber<5) {
    console.log("Sorry, no numbers");
    }
}